//TODO: Make sure a single IP can't submit too many new accounts all in a row. (DOS Attack)

var mail	= require('mailer');

/**
 * Root level routing
 */
module.exports = exports = function(app) {

    if ( app.debug )
        require('colors');
            
    var locals          = app.cfg['default'];
    
    //Indicate the user is registered.
    locals.registered=false;
    
    //Identify the user's email address.
    locals.email    = null;
    
    //Indicate the user's email address is validated.
    locals.trusted  = false;
    
    //Indicate an email validation code was sent.
    locals.codesent = false;
    
    //Configure a default image
    locals.thumbnail = 'http://' + app.cfg.domain + '/images/user.png';


    var getPerson   = function(id,callback) {
                        //Check the cache for user ID based on login method
                        app.getUser(id,function(success,person){
                            
                            //If no person is found, it's a new registration
                            if (!success) {
                                app.registerUser(req,function(user,emailSent){
                                    var eml          = app.getPrimaryEmail(user.eml);
                                    
                                    locals.trusted   = eml.vld   || false;
                                    locals.email     = eml.addr  || null;
                                    locals.codesent  = emailSent || false;
                                    
                                    if (callback)
                                        callback(locals);
                                    else
                                        res.render('index',locals);
                                });
                            } else {
                                /*var eml          = app.getPrimaryEmail(person.eml);
                                    
                                locals.trusted   = eml.vld   || false;
                                locals.email     = eml.addr  || null;
                                */
                               console.log("______________________________".red);
                               console.log(person);
                               console.log(success);
                               
                               if (callback)
                                   callback(locals);
                               else
                                   res.render('index',locals);
                            }
                            
                        });
                    };


    /**
     * Handle posts with email address
     */
    app.post('/', function(req,res){
        
        //Indicate the user is logged in.
        locals.loggedIn = false;
        
        //Make sure the request body has an email address defined.
        if ( typeof req.body.email == undefined ) {
            
            if ( app.debug )
                console.log('Invalid request (no login or email address provided)'.red);
                return app.ErrorPage.display(404);
        } else if (!app.isValidEmailSyntax(usr.email)) {
            //Make sure the email is of valid syntax
            if (app.debug)
                console.log(usr.email.bold.red+' is invalid syntax.'.red);
            return app.ErrorPage.display(400);
        }
        
        if (app.debug)
            console.log('Lookup user '.yellow+req.body.email.toString().yellow.bold+' in cache.'.yellow);
        
        getPerson(req.body.email.toString());
        
    });





	
	
	/**
	 * Handle responses from OAuth services and general page views.
	 */
	app.get('/', function( req, res ){

        //Indicate the user is logged in.
        locals.loggedIn = app.isLoggedIn(req);

		/**
         * If the user is logged in, they have successfully authorized a third party service
         * via OAuth (such as Github, Facebook, Twitter).
         */
		if ( locals.loggedIn ) {
			
			var login   = app.getUserLoginData(req),
                ckey    = login._logintypeabbr+':'+login.id;
			
			//Check for user in Cache
			if (app.debug) {
			    console.log('User is logged in: '.cyan+(locals.loggedIn?locals.loggedIn.toString().green.bold.underline:locals.loggedIn.toString().red.bold.underline));
				console.log('Looking in the cache for the user.'.yellow.bold);
			}
			
			//Check the cache for user ID based on login method
			getPerson(ckey);

		} else
			res.render( 'index', locals );
	});
	
};