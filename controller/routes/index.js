/**
 * Add Routes
 */
module.exports = exports = function( app ){
	
	dir 		= __dirname + '/';
	files 		= require('fs').readdirSync( dir );

	files.forEach( function( file ) {
	
		//Convert filename to array
		var a = file.split('.');
	
		//Export the schema object
		if ( file !== 'index.js' && a[a.length-1].toLowerCase() === 'js' )
			require( dir + a[0] )(app);
	
	});
	
};