module.exports = exports = function(app) {

	//Purges Redis caches (as defined in CacheServer/Cache)
    app.get('/clearcache/:store', function(req,res){
        try {
            console.log("Purging "+req.params.store);
            if(req.params.store == 'all'){
                for (var i=0; i<app.cache.stores.length; i++){
                    console.log(app.cache.stores[i]);
                    app.cache[app.cache.stores[i]].purge();
                }
            }else{
                app.cache[req.params.store].purge();
            }
            res.send(200);
        } catch (e) {
            console.log(e);
            res.send(500);
        }
    });
    
}