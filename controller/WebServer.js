var	cfg			= require('./Configuration'),
	util		= require('./util'),
	global      = require('./global'),
	routes		= require('./routes'),
	modules		= require('./modules'),
	Goose		= require('../model'),
	/*Facebook	= new modules.Facebook({
					appid: cfg.facebook.appId,
					secret: cfg.facebook.appSecret,
					host: cfg.facebook.hostname
				}),
	Twitter		= new modules.Twitter({
					key: cfg.twitter.key,
					secret: cfg.twitter.secret
				}),
	LinkedIn	= new modules.LinkedIn({
					key: cfg.linkedin.key,
					secret: cfg.linkedin.secret
				}),*/
	Github		= new modules.Github({
					key: cfg.github.key,
					secret: cfg.github.secret,
					hostname: 'https://'+cfg.domain
				}),
	Cache		= new modules.CacheServer(),
	Email		= modules.Sendgrid,
	
	fs			= require('fs'),
	express		= require('express'),
	email		= require('mailer'),
	syslog		= util.Syslog,
	dump		= util.Dump;


//Instantiate colors module for pretty console output (debugging)
require('colors');


//Configure common cache stores
Cache.registerStore('user::account');


/**
 * APPLICATION SERVER
 */
var app = express.createServer(
	express.bodyParser(),
	express.cookieParser(),
	express.session({ secret: 'Ev3nt10n4ry' }),
	//Facebook.middleware(),
	//Twitter.middleware(),
	//LinkedIn.middleware(),
	//Github.middleware(),
	function ( req, res, next ) {
		res.removeHeader('X-Powered-By');
		res.header('X-Powered-By','Ecor Systems');
		next();
	}
);



//Configurations
app.configure('dev',function(){
    //Instantiate colors module for pretty console output (debugging)
    require('colors');
    console.log('Running in '.green.bold+'development'.bold.underline.red+' mode.'.green.bold);
    
    app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
    app.use(express.session({ secret: 'Ev3nt10n4ry' }));
    app.use(express.static(cfg.mvc['static']+'/'));
    
    app.debug = true;
});

app.configure('production',function(){
    console.log('Running in production mode.');
    
    app.use(express.errorHandler({dumpExceptions: false, showStack: false}));
    app.use(express.session({ store: new require('connect-redis')(express), secret: 'Ev3nt10n4ry' }));
    app.use(express.static(cfg.mvc['static']+'/',{maxAge: 31557600000}));
    
    app.debug = false;
});

app.use(app.router);
app.set('views', cfg.mvc.view + '/templates' ); //Set layout engine
app.set('view engine', 'ejs');
app.set('view options', { layout: false });


//Add custom error pages
app.Exception   = new modules.CustomError(app,'error/');

/**
 *  -------------------------------- EXTRAS --------------------------------
 */


//Add OAuth Support for specific 3rd Party sites
//Facebook.help(app);
//Twitter.help(app);
//LinkedIn.help(app);
//Github.help(app);

//Pass configuration to app
app.cfg				= cfg; 

//Logging
app.syslog          = syslog;

//Add email support using Sendgrid
app.email			= {
						Server: new Email.Server({
							from:		'support@'+cfg.domain,
							fromname:	'Eventionary',
							replyto:	'support@'+cfg.domain,
							domain: 	cfg.domain,
							username: 	cfg.smtp.username,
							password: 	cfg.smtp.password,
							debug: 		app.debug
						}),
						Message: Email.Message
					};		

//Add database support
app.db				= new Goose({
						server:		cfg.mongo.server,
						port:		cfg.mongo.port,
						database:	cfg.mongo.database,
						username:	cfg.mongo.username,
						password:	cfg.mongo.password,
						debug:		app.debug,
						autoConnect:true
					});


app.logger			= syslog;
app.cache			= Cache;

/**
 *  -------------------------------- END XTRAS --------------------------------
 */



//Add Routes when the DB is initialized
app.db.on('ready',function(){
	global(app,modules._oauth);
	routes(app);
});



//Export as a node module
module.exports = exports = app;