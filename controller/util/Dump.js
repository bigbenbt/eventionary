var sys		= require('util'),
	colors	= require('colors'),
	dumpct	= 0;
	
module.exports = exports = function(code,label) {
	dumpct++;
	if (label)
		console.log('\n'+dumpct.toString().green.underline+' - '.cyan.underline+label.toUpperCase().cyan.underline+' '.cyan.underline);
	else
		console.log('\n'+dumpct.toString().green.underline+' - DUMP'.cyan.underline+' '.cyan.underline);
	sys.puts(sys.inspect(code).yellow);
	console.log('\n');
}