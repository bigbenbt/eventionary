var	cfg				= require('../Configuration'),
	EventEmitter 	= require('events').EventEmitter,
	colors			= require('colors'),
	logger			= require('winston');



logger.add( logger.transports.Loggly, cfg.loggly );
logger.info( '\nLogging to Loggly.com.'.cyan );

module.exports = exports = (logger);