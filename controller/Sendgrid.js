var https		= require('https'),
	email		= require('mailer'),
	events		= require('events');


/**
 * Email Server
 */
function Server( config ) {
	
	config 			= config || {};
	
	this.host		= config.host || 'smtp.sendgrid.net';
	this.port		= config.port || 25;
	this.domain		= config.domain || 'localhost';
	this.username	= config.username || null;
	this.password	= config.password || null;
	this.debug		= config.debug || false;
	this.from		= config.from || null;
	this.fromname	= config.fromname || null;
	this.replyto	= config.replyto || null;
		
	if ( this.debug )
		require('colors');
		
	events.EventEmitter.call(this);

};

require('util').inherits(Server, events.EventEmitter);
Server.prototype.__proto__ = events.EventEmitter.prototype;




/**
 * Email Message
 */
function Message( config ) {
	
	this.server		= config.server || new Server();
	this.to			= config.to || [];
	this.toname		= config.toname || [];
	this.cc			= config.cc || [];
	this.bcc		= config.bcc || [];
	this.from		= config.from || this.server.from;
	this.fromname	= config.fromname || this.server.fromname;
	this.replyto	= config.replyto || this.server.replyto || this.from;
	this.subject	= config.subject || 'No Subject';
	this.text		= config.text || null;
	this.html		= config.html || this.text;
	this.template	= config.template || null;
	this.data		= config.data || null;
	
	//TODO: Support Attachments
	//this.attachments= config.attachments || [];
	this.debug		= config.debug || this.server.debug || false;
	
	if ( this.debug )
		require('colors');
	
	//Support for templates
	if (this.template !== null)
	   setTemplate(this.template);
	
	//Convert "to" to an array
	if (this.to.constructor.toString().indexOf('Array') == -1)
		this.to = [this.to];
		
	//Convert "toname" to an array
	if (this.toname.constructor.toString().indexOf('Array') == -1)
		this.toname = [this.toname];
		
	//Convert "cc" to an array
	if (this.cc.constructor.toString().indexOf('Array') == -1)
		this.cc = [this.cc];
		
	//Convert "bcc" to an array
	if (this.bcc.constructor.toString().indexOf('Array') == -1)
		this.bcc = [this.bcc];
		
	var tmp = this.to.concat(this.cc,this.bcc);
	
	if (this.from !== null)
		tmp.push(this.from);
	
	//Validate email address syntax
	for (var i=0; i<tmp.length; i++) {
		if ( !this.validSyntax(tmp[i]) )
			throw InvalidAddressSyntaxException(tmp[i]);
	}
	
	if ( this.debug && typeof config.server == undefined )
		console.log('ALERT: '.bold.red+'No email server specified. '.red+'Using default (localhost)'.underline.yellow);
	
	events.EventEmitter.call(this);
}

require('util').inherits(Message, events.EventEmitter);
Message.prototype.__proto__ = events.EventEmitter.prototype;


/**
 * Verifies email address adheres to RFC 2822 standard syntax.
 */
Message.prototype.validSyntax = function( addr ) {
    var re = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    
    return re.test(addr);
};



Message.prototype.setTemplate = function(src,encoding,data) {
    
    data        = data || encoding != 'object' ? encoding : null;
    encoding    = typeof encoding == 'object' ? 'utf8' : (encode || 'utf8');
    

    if (data !== null)
        this.setData(data);

    if ( this.debug )
        console.log('Setting template for '.cyan+src.cyan);
        
    //Initialize the rendering engine (once)
    this.mustache = require('mustache');
       
    //If the template is a filepath, get the content of the file.
    try { 
        this.template = require('fs').readFileSync(src,encoding);
    } catch (e) {
        
        if ( this.deug )
            console.log('Could not read the template at '.red+src.red+' - Looking for multiple template files.'.yellow);
        
        var _html = null,
            _text = null;
        
        //If the template has separate files for text and HTML, modify the template object.
        try {
            _html = require('fs').readFileSync(src+'.html.mustache',encoding);
            
            if ( this.debug )
                console.log(src.cyan.bold+'.html.mustache'.cyan.underline+' found and processed.'.cyan.bold);
        } catch(e) {}
        
        try {
            _text = require('fs').readFileSync(src+'.text.mustache',encoding);
            
            if ( this.debug )
                console.log(src.cyan.bold+'.text.mustache'.cyan.underline+' found and processed.'.cyan.bold);
        } catch(e) {}
        
        //If a file is found, update the template 
        if ( _html !== null || _text !== null ) {
            this.template = { root: src };
            
            if ( _html !== null )
                this.template.html = _html;
            
            if ( _text !== null )
                this.template.text = _text;
        } else
            this.template = src;
            
    }
}


/**
 * @cfg {Object} values An object of key/values used to render a template.
 * @cfg {type} [type] Type of email to send. Valid values include "html", "text", or "both". Defaults to "text".
 * Sending both will send a multipart message with text and HTML.
 */
Message.prototype.render = function( values, type ) {
    
    var _html   = null, 
        _text   = null,
        data    = 'text,html,both'.indexOf(values) == -1 ? values || this.data : this.data,
        type    = 'text,html,both'.indexOf(values) >= 0 ? values : type || 'text';      //Defaults to text document
    
    //Render the content
    if (typeof this.template.root === undefined) {
        _text = this.mustache.to_html(this.template,data);
    } else {
        if (typeof this.template.html !== undefined)
            _html = this.mustache.to_html(this.template.html,data);
        if (typeof this.template.html !== undefined)
            _text = this.mustache.to_html(this.template.text,data);
    }
    
    //Apply the content to local variables    
    if (type == 'both' || type == 'html')
        this.setHtml( _html || _text );
    if (type == 'both' || type == 'text')
        this.setText( _text || _html );
    
}



Message.prototype.setData = function( data ) {
    this.data = (data == null || data == undefined) 
                ? {} 
                : ( typeof data == 'object'
                    ? data
                    : {}
                  );
                  
     if ( this.debug ) {
        if ( typeof this.data !== 'object')
            console.log('Data passed to template is not a valid object'.red.bold.underline);
     }
}


/**
 * @cfg {String|Array} addr An email address or array of addresses.
 * @cfg {String|Array} name A name or array of names corresponding to addr.
 */
Message.prototype.setTo = function( addr, name ) {
	
	addr = addr || null;
	name = name || null;
	
	this.to 	= addr == null ? this.to : (addr.constructor.toString().indexOf('Array') == -1 ? [addr] : addr);
	this.toname = name == null ? this.toname : (name.constructor.toString().indexOf('Array') == -1 ? [name] : name);
	
	//Make sure addresses are valid syntax
	for (var i=0; i < this.to.length; i++) {
		if ( !this.validSyntax(this.to[i]) )
			throw InvalidAddressSyntaxException(this.to[i]);
	}
	
};



Message.prototype.setCC = function( addr ) {
	
	this.cc 	= addr.constructor.toString().indexOf('Array') == -1 ? [addr] : addr;
	
	//Make sure addresses are valid syntax
	for (var i=0; i < this.cc.length; i++) {
		if ( !this.validSyntax(this.cc[i]) )
			throw InvalidAddressSyntaxException(this.cc[i]);
	}
	
};



Message.prototype.setBCC = function( addr ) {
	
	this.bcc 	= addr.constructor.toString().indexOf('Array') == -1 ? [addr] : addr;
	
	//Make sure addresses are valid syntax
	for (var i=0; i < this.bcc.length; i++) {
		if ( !this.validSyntax(this.bcc[i]) )
			throw InvalidAddressSyntaxException(this.bcc[i]);
	}
	
};



Message.prototype.setFrom = function( addr, name ) {
	this.from 		= addr;
	this.fromname 	= name;
	
	//Validate from name syntax
	if ( !this.validSyntax(this.from) )
		throw InvalidAddressSyntaxException(this.from);
};



Message.prototype.setReplyTo = function( addr) {
	this.replyto	= addr;
	
	//Validate from name syntax
	if ( !this.validSyntax(this.replyto) )
		throw InvalidAddressSyntaxException(this.replyto);
};



Message.prototype.setSubject = function( sbj ) {
	this.subject = sbj || '';
	this.subject = this.subject.trim();
	
	if (this.subject.length == 0)
		this.subject('No Subject');
};



Message.prototype.setText = function( txt ) {
	this.text = txt || this.text;
};



Message.prototype.setHtml = function( htm ) {
	this.html = htm || this.html;
};



Message.prototype.send = function(callback) {

	var self = this;
	
	//If the "to" attribute is an array and "toname" is defined, they must both be
	//arrays of equal length.
	if (this.to.length != this.toname.length && this.toname.length > 0)
		throw InvalidAddressException('','When \"to\" and \"toname\" are specified, they must both be a string or both be arrays of equal length.');
	
	//If CC is specified, combine with "to"
	if (this.cc.length > 0) {
		
		this.to.concat(this.cc);
		
		//Make sure the names match up so Sendgrid doesn't fail. (if applicable)
		if(this.toname.length > 0) {
			for(i=0; i<this.toname.length; i++) {
				this.toname.push('');
			}
		}
	}
	
	//Make sure there are some address to send/receive
	if (this.to.length == 0 || this.from == null)
		throw InvalidAddressException('','TO or FROM is invalid or could not be found.'); 
	
	//Make sure security credentials are provided
	if (this.server.username == null || this.server.password == null)
		throw UnauthorizedException();
	
	var sgu = new Buffer(this.server.username),
		sgp = new Buffer(this.server.password);
	
	//Make sure there is a message
	if (this.text == null && this.html == null) {
		
		//If no template is defined, then no message has been defined at all.
		if (this.template == null || this.data == null) {
    		if (this.data == null && this.template != null)
    		  throw MissingMessageException('No data was provided to the template.');
    		
    		throw MissingMessageException();
        }
    	
    	//Process a non-rendered template as text.
    	this.render('both');
   	}
	
		
	/** 
	 * First attempt a REST-based request
	 */
	//Configure the to/toname fields
	var _to = [],
		_bcc= [];
	for(var i=0; i<this.to.length; i++) {
		
		var str = 'to[]='+this.to[i].trim();
		
		if (this.toname.length > 0)
			str = str + '&toname[]='+encodeURIComponent(this.toname[i].trim()); 
		 
		_to.push(str);
	}
	
	//Add CC
	for(var i=0; i<this.cc.length; i++) {
		
		var str = 'to[]='+this.cc[i].trim();
		
		if (this.toname.length > 0)
			str = str + '&toname[]='+encodeURIComponent(this.cc[i].trim()); 
		 
		_to.push(str);
	}
	
	//Add BCC
	for(var i=0; i<this.bcc.length; i++)
		_bcc.push('bcc[]='+this.bcc[i].trim());
	
	
	var httpsResponse 	= '',
		qs				= '/api/mail.send.json?'
						+'api_user='+this.server.username
						+'&api_key='+this.server.password
						+'&'+_to.join('&')
						+(this.bcc.length > 0 ? '&'+_bcc.join('&') : '')
						+'&subject='+encodeURIComponent(this.subject)
						+(this.text != null ? '&text='+encodeURIComponent(this.text) : '')
						+(this.html != null ? '&html='+encodeURIComponent(this.html) : '')
						+(this.fromname.trim().length>0?'&fromname='+encodeURIComponent(this.fromname):'')
						+'&from='+this.from
						+(this.replyto != null ? '&replyto='+this.replyto : '')
						;
						
	var eml 	= https.request({
					host: 'sendgrid.com',
					port: 443,
					method: 'GET',
					path: qs
				},function(res){
					
					res.setEncoding('utf8');
					
					res.on('data',function(chunk){
						httpsResponse += chunk;
					});
					
					res.on('end',function() {
						
						if ( self.debug )
							console.log('Email successfully sent via REST.'.green);
							
						if ( typeof callback == 'function')
							callback(httpsResponse);
						
					});
				});
				
	//In the event of an error with the REST request, attempt via SMTP (using node mailer)	
	eml.on('error', function(e){

		if ( self.debug ) {
			console.log('Sendgrid REST API '.red.bold+'FAILURE'.red.bold.underline);
			console.log(e);
		}
			
		var obj = {
					host:			self.server.host,
					port:			self.server.port,
					domain:			self.server.domain,
					subject:		self.subject,
					body:			self.html || self.text,
					from:			self.from,
					username:		self.server.username,
					password:		self.server.password,
					authentication: 'login'
				};

		//If a template is defined, use it
		if (self.template !== null ) {
			obj.template 	= self.template;
			obj.data 		= self.data;
		}
		
		//If a replyTo is available, use it.
		if (self.replyto !== null)
			obj.reply_to = self.replyto;
			
		//Loop through all of the messages and 
		var all = self.to.concat(self.cc,self.bcc);
	
		//Send an individual message to each address
		var response	= [];
		for (var i=0; i<all.length; i++) {
			obj.to = all[i];

			//Send the message
			email.send(obj,
			function(err, result){
				
				//Throw an error if one exists.
				if (err) throw err;
				
				//Add the result to the response array.
				response.push({ to:obj.to, success: result });
				
				//If this is the last message, execute the callback
				if(response.length == all.length) {
					
					if (typeof callback == 'function')
						callback(response);
					
					if ( self.debug )
						console.log('Email successfully sent via '.green+'SMTP'.bold.green+'.'.green);
				}
			});	
		}
	});
	
	eml.end();
	
}



/**
 * Error Objects
 */

//Standard Message Exception
function MessageException( config ) {
	Error.call(this);
	Error.captureStackTrace(this, arguments.callee);
	this.type		= config.type		|| 'General';
	this.name 		= config.name 		|| 'Error';
	this.message	= config.message 	|| 'Unknown Email Message Exception.';
	this.detail		= config.detail		|| '';
	this.timestamp	= config.timestamp	|| Date.now;
};

MessageException.prototype.__proto__ = Error.prototype;

function InvalidAddressException(addr,err) {
	
	addr = addr || '';
	
	return new MessageException({
		type:		'Address',
		name:		'InvalidAddress',
		message:	addr.trim().length > 0 ? 'The address \"'+addr+'\" is either invalid or used in an invalid way.' : 'Invalid address or combination of addresses.',
		detail:		err || null
	});
};

InvalidAddressException.prototype = MessageException.prototype;

function InvalidAddressSyntaxException(addr) {
	
	addr = addr || '';
	
	return new MessageException({
		type:		'Address',
		name:		'InvalidAddress',
		message:	addr.trim().length > 0 ? 'The address \"'+addr+'\" is eiher invalid or used in an invalid way.' : 'Invalid address or combination of addresses.',
		detail:		'Invalid syntax.'
	});
};

InvalidAddressSyntaxException.prototype = MessageException.prototype;

function UnauthorizedException(msg) {
	return new MessageException({
		type:		'Security',
		name:		'Unauthorized',
		message:	'The credentials are either missing or invalid.',
		detail:		msg || null
	});
};

UnauthorizedException.prototype = MessageException.prototype;

function MissingMessageException(msg) {
	return new MessageException({
		type:		'General',
		name:		'Missing Body',
		message:	'No body (text or HTML) was supplied. A blank message will not be sent.',
		detail:		msg || null
	});
};

MissingMessageException.prototype = MessageException.prototype;


//Standard Server Exception
function ServerException( config ) {
	Error.call(this);
	Error.captureStackTrace(this, arguments.callee);
	this.type		= config.type		|| 'General';
	this.name 		= config.name 		|| 'Error';
	this.message	= config.message 	|| 'Unknown Email Server Exception.';
	this.detail		= config.detail		|| '';
	this.timestamp	= config.timestamp	|| Date.now;
};

ServerException.prototype.__proto__ = Error.prototype;


/**
 * Module Exports
 */
module.exports = exports 	= {
								Server: Server,
								Message: Message	
							};