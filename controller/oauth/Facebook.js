var	EventEmitter= require('events').EventEmitter,
	auth		= require('everyauth'),
	FB			= {};
	

module.exports = exports = Facebook;


//Primary Object
function Facebook( config ) {
	
	this.users	= {};
	this.appid	= config.appid || null;
	this.secret	= config.secret || null;
	this.host	= config.host || null;
	this.timeout= config.timeout || 8; //seconds
	this.redirect=config.redirect || '/';
	
	
	/**
	 * AUTHORIZATION
	 */
	auth.users = {};
	auth.facebook.moduleTimeout(this.timeout*1000);
	auth.debug = true;
	
	//Facebook
	auth
	  .facebook
	    .myHostname('http://'+this.host)
	    .appId(this.appid)
	    .appSecret(this.secret)
	    .handleAuthCallbackError( function( req, res ){
			res.redirect(this.redirect);
	    })
	    .findOrCreateUser( function (session, accessToken, accessTokenExtra, fbUserMetadata) {
	    	return 	auth.users[fbUserMetadata.id] ||
	        		(auth.users[fbUserMetadata.id] = fbUserMetadata);
	    })
	    /*.callbackPath(function( req, res ){
	    	return "/";
	    })*/
	    .redirectPath('/')
	    .scope(function( req, res ){
	    	
	    	var session = req.session;
	    	
	    	switch(session.service) {
	    		
	    		//Basic site access
	    		default:
	    			return 'email';
	    	}
	    	/*,user_likes,user_status,read_mailbox,publish_stream,offline_access*/
	    });
};

Facebook.prototype.__proto__ = EventEmitter.prototype;

Facebook.prototype.middleware = auth.middleware;
Facebook.prototype.help = function(app) {
	this.webserver = app;
	auth.helpExpress(this.webserver);
}
Facebook.prototype.getUsers = function() {
	return auth.users;
};