var	EventEmitter= require('events').EventEmitter,
	auth		= require('everyauth');
	

module.exports = exports = Github;


//Primary Object
function Github( config ) {
	
	this.users		= {};
	this.key		= config.key || null;
	this.secret		= config.secret || null;
	this.host		= config.host || null;
	this.timeout	= config.timeout || 8; //seconds
	this.redirect	= config.redirect || '/';
	this.debug		= config.debug || false;
	this.hostname	= config.hostname || undefined;
	
	
	/**
	 * AUTHORIZATION
	 */
	auth.users = {};
	auth.github.moduleTimeout(this.timeout*1000);
	auth.debug = this.debug;
	
	//Github
	if ( typeof this.hostname !== undefined ) {
		auth
		  .github
		    .appId(this.key)
		    .appSecret(this.secret)
		    .myHostname(this.hostname)
		    .handleAuthCallbackError( function( req, res ){
				res.redirect(this.redirect);
		    })
		    .findOrCreateUser( function (session, accessToken, accessTokenSecret, githubUserMetadata) {
		    	return 	auth.users[githubUserMetadata.id] ||
		        		(auth.users[githubUserMetadata.id] = githubUserMetadata);
		    })
		    .redirectPath(this.redirect);
	} else {
		auth
		  .github
		    .appId(this.key)
		    .appSecret(this.secret)
		    .handleAuthCallbackError( function( req, res ){
				res.redirect(this.redirect);
		    })
		    .findOrCreateUser( function (session, accessToken, accessTokenSecret, githubUserMetadata) {
		    	console.log(githubUserMetadata);
		    	return 	auth.users[githubUserMetadata.id] ||
		        		(auth.users[githubUserMetadata.id] = githubUserMetadata);
		    })
		    .redirectPath(this.redirect);
	}

};

Github.prototype.__proto__ = EventEmitter.prototype;

Github.prototype.middleware = auth.middleware;
Github.prototype.help = function(app) {
	this.webserver = app;
	auth.helpExpress(this.webserver);
}
Github.prototype.getUsers = function() {
	return auth.users;
};