var	EventEmitter= require('events').EventEmitter,
	auth		= require('everyauth');
	

module.exports = exports = LinkedIn;


//Primary Object
function LinkedIn( config ) {
	
	this.users	= {};
	this.key	= config.key || null;
	this.secret	= config.secret || null;
	this.host	= config.host || null;
	this.timeout= config.timeout || 8; //seconds
	this.redirect=config.redirect || '/';
	
	
	/**
	 * AUTHORIZATION
	 */
	auth.users = {};
	auth.linkedin.moduleTimeout(this.timeout*1000);
	auth.debug = true;
	
	//Facebook
	auth
	  .linkedin
	    .consumerKey(this.key)
	    .consumerSecret(this.secret)
	    .handleAuthCallbackError( function( req, res ){
			res.redirect(this.redirect);
	    })
	    .findOrCreateUser( function (session, accessToken, accessTokenSecret, linkedinUserMetadata) {
	    	return 	auth.users[linkedinUserMetadata.id] ||
	        		(auth.users[linkedinUserMetadata.id] = linkedinUserMetadata);
	    })
	    /*.callbackPath(function( req, res ){
	    	return "/";
	    })*/
	    .redirectPath(this.redirect);
};

LinkedIn.prototype.__proto__ = EventEmitter.prototype;

LinkedIn.prototype.middleware = auth.middleware;
LinkedIn.prototype.help = function(app) {
	this.webserver = app;
	auth.helpExpress(this.webserver);
}
LinkedIn.prototype.getUsers = function() {
	return auth.users;
};