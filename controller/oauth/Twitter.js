var	EventEmitter= require('events').EventEmitter,
	auth		= require('everyauth');
	

module.exports = exports = Twitter;


//Primary Object
function Twitter( config ) {
	
	this.users	= {};
	this.key	= config.key || null;
	this.secret	= config.secret || null;
	this.host	= config.host || null;
	this.timeout= config.timeout || 8; //seconds
	this.redirect=config.redirect || '/';
	
	
	/**
	 * AUTHORIZATION
	 */
	auth.users = {};
	//auth.twitter.moduleTimeout(this.timeout*1000);
	auth.debug = true;

	//Facebook
	auth
	  .twitter
	  	.consumerKey(this.key)
	    .consumerSecret(this.secret)
	    .callbackPath('/auth/twitter/callback/')
	    .findOrCreateUser( function (session, accessToken, accessTokenSecret, twitterUserMetadata) {
	      return 	auth.users[twitterUserMetadata.id] || 
	      			(auth.users[twitterUserMetadata.id] = twitterUserMetadata);
	    })
	    .redirectPath('/');
	    
};

Twitter.prototype.__proto__ = EventEmitter.prototype;

Twitter.prototype.middleware = auth.middleware;
Twitter.prototype.help = function(app) {
	this.webserver = app;
	auth.helpExpress(this.webserver);
}
Twitter.prototype.getUsers = function() {
	return auth.users;
};