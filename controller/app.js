var	cfg			= require('./Configuration'),
	//http		= require('http'),
	//httpProxy	= require('http-proxy'),
	web			= require('./WebServer'),
	syslog		= require('./util').Syslog; //Starts logging services.


//Launch the application server
web.listen(cfg.port.web);
syslog.info('Web server listening on port '+cfg.port.web);
/*
proxyserver.listen(cfg.port.proxy);
syslog.info('Proxy server listening on port '+cfg.port.proxy);
*/