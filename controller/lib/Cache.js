var	events      = require('events'),
	redis		= require('redis');

/**
 * Represents a cache (Redis-based).
 * @param {Object} server The Redis server connection.
 * @param {String} name The name of the cache.
 * @param {Number} [ttl] Time to live. The default amount of time (in seconds) before the record is automatically expired. Defaults to never expire.
 * @property {Object} server The Redis server connection.
 * @property {String} name The name of the cache.
 * @property {Boolean} authorized Indication the connection has been authorized.
 * @property {Object} client The Redis client.
 * @class
 */
function Cache( cacheserver, name, ttl ) {
	
	//Server Configuration
	this.server 	= 	{
							host:		cacheserver.host || '127.0.0.1',
							port:		cacheserver.port || 6379,
							password:	cacheserver.password || null
							
						};
	
	//Local client configuration (connect to server)
	this.client		= 	redis.createClient( 
							this.server.port,
							this.server.host
						);
	
	//Turn on/off debugging				
	this.client.debug_mode = false;
	
	//A list of known keys in the cache
	this.knownKeys		= {};
	
	this.authorized	= false;
	this.expire		= ttl || 0;
	
	//Make sure the cache has a simple name.
	this.name		= name.replace(/[^\w\s\:]/gi,'').trim();

	//If a password is configured, use it to authorize the connection.
	if ( typeof this.server.password !== undefined ) {
		this.client.auth( this.server.password, function(){
			this.authorized = true;
		});
	}
	
	//Log errors
	this.client.on("error", function (err) {
	    app.syslog.error(err);
	});
	
	//HELPER METHODS
	this.key = function( keyName ) {
	    console.log('====================================================');
	    console.log(keyName);
        console.log('====================================================');
		var key = '_c_'+this.name+':'+keyName.toString().toLowerCase();
		
		return key; 
	}
	
	this.addKnownKey = function( key ) {
		this.knownKeys[this.key(key)] = null;
	};
	
	this.removeKnownKey = function( key ){
		delete this.knownKeys[this.key(key)];
	}
	
	events.EventEmitter.call(this);
};

require('util').inherits(Cache, events.EventEmitter);
Cache.prototype.__proto__ = events.EventEmitter.prototype;



/**
 * Forcibly close the cache/Redis connection.
 */
Cache.prototype.close = function() {
	this.client.quit();
};


/**
 * Supports caching of objects (without functions).
 * Also supports expiration.
 * @param {String} key The key name of the cache record.
 * @param {Object} value The value to cache - either an object or string.
 * @param {Number} [ttl] Time to live for the record. The record will automatically expire after this. Value in seconds.
 * @return void
 */
Cache.prototype.put = function( key, value, ttl ) {
	
	var exp = ttl || this.expire,
		val = typeof value === 'object' ? JSON.stringify(value) : value,
		key = this.key(key);

	if ( exp > 0 )
		this.client.setex( key, exp, val );
	else
		this.client.set( key, val );
		
	//Add to the list of known keys
	this.addKnownKey( key );
};


/**
 * Get a cache key.
 * @example
 * Cache.get('mykey',function(err,reply){
 * 		console.log(reply);
 * });
 * @param {String} key The key of the cache record to return.
 * @param {Function} callback The callback function executed when a result is returned.
 * This function receives the reply argument, which is null if no value is found.
 * If a value is found, it may be a string or object.
 * @return void 
 */
Cache.prototype.get = function( key, callback ) {
	
	this.client.get( this.key(key), function( err, reply ){
		if (err)
			throw err;//callback( null );
		else {
		    var result = null;
		    try {
		        result = JSON.parse(reply);
		    } catch (e) {
		        result = reply;
		    }
			callback( result );
		}
	});
	
};



/**
 * Remove a cache value.
 * @param {String} key The key to remove fro mthe cache.
 * @return void
 */
Cache.prototype.remove = function( key ) {
	var self = this;
	
	this.client.del( this.key(key), function(err, data){
		if (err) throw err;
		self.removeKnownKey(key);
	});
};


/**
 * Purge all known keys from the cache server.
 * This method will not remove unknown keys that may be available before the cache is loaded.
 * A full purge may happen over time. If unknown keys exist, they will be removed only when
 * their TTL expires.
 */
Cache.prototype.purge = function() {
	
	console.log('Flushing...'.cyan);
	this.client.flushdb();
	
	//Remove each key one by one. Ignores errors and missing keys.
	for ( key in this.knownKeys ) {
	    console.log('Purging '+this.key(key));
		this.client.del(this.key(key));
	}
	
	//Force purge
	this.knownKeys = {};
};


/**
 * Module Exports
 */

module.exports = exports = Cache;