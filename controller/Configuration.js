var	EventEmitter= require('events').EventEmitter;


//Exports
module.exports = exports = new Configuration();


//Primary Object
function Configuration() {
	
	this.domain		=		'@TLD';
	this.root		=		'/web/websites/'+this.domain+'/api';
	this.ssl		=		this.root + '/ssl/';
	this.ttl		=		false;
	
	//Ports used throughout the application
	this.port		=		{
								web:	443
							};
							
	//MVC Pattern								
	this.mvc		=		{
								model:		this.root+'/model',
								view:		this.root+'/view',
								'static':   this.root+'/view/public',
								controller:	this.root+'/controller'
							};
							
	//Defaults
	this['default']	=		{
								title:		        'acct.io',
								isMobile:	        false,
								loggedIn:	        false,
								requirePassword:    false
							};
							
	this.template	=		{
								mail:		this.root+'/view/templates/email/'
							};
							
	this.mongo		=		{
								server:		'@SERVICE.MONGO.SERVER',
								port:		@SERVICE.MONGO.PORT,
								username:	'@SERVICE.MONGO.USERNAME',
								password:	'@SERVICE.MONGO.PASSWORD',
								database:	'@SERVICE.MONGO.DATABASE'
							};
									
	this.facebook	=		{
								appId:			'@SERVICE.FACEBOOK.APPID',
								appKey:			'@SERVICE.FACEBOOK.APPKEY',
								appSecret:		'@SERVICE.FACEBOOK.SECRET',
								hostname:		'www.'+this.domain,
								timeout:		8
							};
	
	this.twitter	=		{
								key:			'@SERVICE.TWITTER.KEY',
								secret:			'@SERVICE.TWITTER.SECRET',
								timeout:		8
							};
							
	this.github		=		{
								key:			'@SERVICE.GITHUB.KEY',
								secret:			'@SERVICE.GITHUB.SECRET',
								timeout:		8
							};
							
	this.linkedin	=		{
								key:			'@SERVICE.LINKEDIN.KEY',
								secret:			'@SERVICE.LINKEDIN.SECRET',
								timeout:		8
							};
							
	this.foursquare	=		{
								appID:			'@SERVICE.FOURSQUARE.APPID',
								appSecret:		'@SERVICE.FOURSQAURE.SECRET'
							};
			
	//Logging			
	this.loggly		=		{
								subdomain:		'@SERVICE.LOGGLY.SUBDOMAIN',
								auth: {
									username:	'@SERVICE.LOGGLY.USERNAME',
									password:	'@SERVICE.LOGGLY.PASSWORD'
								},
								inputToken:		'@SERVICE.LOGGLY.INPUT',
								level:			'info'
							};
			
	//Email
	this.smtp		=		{
								server:		'@SERVICE.SMTP.SERVER',
								username:	'@SERVICE.SMTP.USERNAME',
								password:	'@SERVICE.SMTP.PASSWORD',
								port:		'@SERVICE.SMTP.PORT',
								from:		'notice@'+this.domain,
								ssl:		@SERVICE.SMTP.SSL,
								templates:  this.mvc.view+'/templates/email/'
							};
};

//Configure as an Event Emitter
Configuration.prototype.__proto__ = EventEmitter.prototype;