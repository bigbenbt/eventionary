var events      = require('events'),
    path        = require('path');;
    
/**
 * Custom Error Module
 * This module is a shortcut for rendering custom and static error pages.
 * @cfg {Object} app The application to apply the error pages to.
 * @cfg {String} [tplPath] The template directory path where custom error templates reside. Considers the root
 * to be wherever your Express server is configured to look for templates.
 * defined for templates if none is specified. 
 */
function CustomErrorPages( app, tplPath ) {
	
	this.syslog      = app.syslog || (require('winston'));
	this.debug       = app.debug || false;
	this.path        = tplPath || '';
	this.settings    = app._locals.settings;
		
	if ( this.debug )
		require('colors');
		
	events.EventEmitter.call(this);

};

require('util').inherits(CustomErrorPages, events.EventEmitter);
CustomErrorPages.prototype.__proto__ = events.EventEmitter.prototype;


/**
 * Generates a custom error page if one is found, otherwise the error is returned as normal.
 * @cfg {Object} stream The response stream.
 * @cfg {Number} httpcode The HTTP status code. If none is speciifed, a 500 code is sent. 
 * @cfg {Object} [args] A key/value object passed to the template (similar to locals for EJS)
 */ 
CustomErrorPages.prototype.show = function( stream, httpcode, args ) {
    
    
    //Set defaults
    args            = args || {};
    httpcode        = httpcode || 500;
    
    
    //Make sure the HTTP code is valid
    if (typeof httpcode !== 'Number')
        httpcode = parseInt(httpcode) !== NaN ? parseInt(httpcode) : 500;
    
    if ( typeof args.title === 'undefined' )
        args.title = 'Error '+httpcode.toString();
  
  
    //Look for a template and fallback to a standard error.
    if (path.existsSync(this.settings.views+'/'+this.path+httpcode+'.'+this.settings['view engine']))
        stream.render(this.path+httpcode,args);
    else {
        //Searches for a custom message or detail string to provide a description of the error. 
        console.log('Sent '+httpcode);
        stream.send(httpcode);
    }
    
    return;
};


/**
 * Module Exports
 */
module.exports = exports = CustomErrorPages;