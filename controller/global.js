/**
 * Add User helper methods
 * @cfg {Object} app The application (express server) to apply functions
 * @cfg {Array} oauth An array of available OAuth modules
 */
module.exports = exports = function( app, oauth ){
    
    if ( app.debug )
        require('colors');
    
    oauth = oauth || [];
    
    /**
     * USER SECURITY FUNCTONS
     */
    app.isLoggedIn      = function( request ) {
                            
                            return  request.session.auth !== undefined 
                                    ? request.session.auth.loggedIn 
                                    : false;
                        };
                        
    app.accessToken     = null;
    
    app.getAccessToken  = function( req ) {
                            if (typeof req !== undefined )
                                app.setAccessToken(req);
                            return app.accessToken;
                        };
    
    app.setAccessToken  = function( req ) {
                            if ( typeof req == 'String' )
                                app.accessToken = req;
                            else {
                                
                                app.accessToken = null;
                                
                                //Loop through the modules to get the appropriate login
                                for (var i=0; i < oauth.length; i++) {
                                    try {
                                        app.accessToken = req.session.auth[modules._auth[i]].accessToken;
                                        break;
                                    } catch (e) {}
                                }
                            }
                        };
                        
    app.getUserLoginData= function( req ) {
    
                            //Loop through login methods.
                            for (var i=0; i < oauth.length; i++) {
                                try {
                                    
                                    var rtn         = req.session.auth[oauth[i]].user;
                                    rtn.accessToken = req.session.auth[oauth[i]].accessToken;
                                    rtn._logintype  = oauth[i].trim();
                                    rtn._logintypeabbr= app.getLoginTypeAbbreviation(rtn._logintype);
                                    
                                    return rtn;
                                    
                                } catch (e) {}
                            }
                            
                            return null;
                        };
                        
    app.getLoginType    = function( req ){
                            
                            //Loop through login methods.
                            for (var i=0; i < oauth.length; i++) {
                                try {
                                    var rtn = req.session.auth[oauth[i]].user;
                                    return oauth[i];
                                } catch (e) {}
                            }
                            
                            return null;
                        };  
    
    app.getLoginTypeAbbreviation= function(type) {
                        
                            if (type == undefined)
                                return null;
                        
                            switch (type.trim().toLowerCase()) {
                                
                                case 'github':
                                    return 'gh';
                                case 'facebook':
                                    return 'fb';
                                case 'linkedin':
                                    return 'li';
                                case 'twitter':
                                    return 'tw';
                                case 'google':
                                    return 'gg';
                                case 'openid':
                                    return 'oi';
                                case 'yahoo':
                                    return 'yh';
                                case 'dropbox':
                                    return 'db';
                                default:
                                    return null;                            
                            }
                            
                        };
    
    app.createUniqueCode= function( length ) {
                            
                            //Default the length requested to zero (if none specified)
                            var length = parseInt(length) || 0;
                       
                            //Generate the code
                            var uuid = require('node-uuid'),
                                code = uuid.v4().replace(/[^a-zA-Z0-9]/g,''),
                                limiter = 0;

                            //If the code generated is shorter than the requested length, add more.
                            while ( code.length < length && length > 0 ) {
                                code += uuid.v4().replace(/[^a-zA-Z0-9]/g,'');
                                limiter++;
                                
                                if (limiter > 10)
                                    break;
                            }
                            
                            //If the length requeste dis greater than the length provided, shorten the result.
                            if ( length < code.length )
                                code = code.substring(0,length);
                            
                            if ( app.debug ) {
                                console.log('Unique Code Requested:'.cyan);
                                console.log('Length Requested: '.yellow+length.toString().cyan.bold);
                                console.log('Length Received: '.yellow+(length.toString() != code.length ? code.length.toString().red.bold : code.length.toString().cyan.bold));
                                console.log('Generated unique code: '.green+code.green.bold);
                                if (limiter > 10)
                                    console.log('Limiter invoked due to infinite loop or too many cycles.'.red.bold);
                                
                            }
                                
                            return code;
                        };
                        
    app.isValidEmailSyntax= function(addr) {
                            var re = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
                            return re.test(addr);
                        };
                        
    //Accepts an array of EmailAddress objects                    
    app.getPrimaryEmail	= function(addr) {
					    	if (addr.length == 1)
					    	  return addr[0];
					    	  
					    	for(var i=0; i<addr.length; i++) {
					    		if (addr[i].prm)
					    			return addr[i];
					    	}
					    };		    
};