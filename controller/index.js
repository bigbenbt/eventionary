/**
 * This is a dynamic loader that includes all of the data model objects.
 */
var fs		= require('fs'),
	auth 	= fs.readdirSync( __dirname + '/oauth' ),
	files	= fs.readdirSync( __dirname ),
	_auth	= [];

//Get all the authorization libraries.
auth.forEach( function( file ) {

	//Convert filename to array
	a = file.split('.');

	//Export the schema object
	if ( file !== 'index.js' && a[a.length-1].toLowerCase() === 'js' ) {
		exports[a[0]] = require( './oauth/' + a[0]);
		_auth.push(a[0].trim().toLowerCase());
	}

});

//Get all of the other libraries (root level only)
files.forEach( function( file ) {

	//Convert filename to array
	a = file.split('.');

	//Export the schema object
	if ( file !== 'index.js' && a[a.length-1].toLowerCase() === 'js' )
		exports[a[0]] = require( './' + a[0]);

});

exports._oauth = _auth;