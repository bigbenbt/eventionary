var	events		= require('events'),
	Cache		= require('./lib/Cache');
	

//module.exports = exports = CacheServer;

/**
 * Connects to a Redis cache server.
 * @example
 * var server = new CacheServer();
 * var server2= new CacheServer({
 * 		host: 'my.host.com',
  * }); 
 * @param {String} [config.host] The host name or IP address of the cache server. Defaults to 127.0.0.1.
 * @param {Number} [config.port] The port number where the Redis service is running. Defaults to 6379 (Redis default).
 * @param {Object} [config.options] An object containing options to pass to the server. 
 * @param {Number} [config.ttl] The default time to live for a cache record. 
 * There are two attributes, parser and return_buffers. The parse can be set to 
 * 'hiredis' (if it is installed) or 'javascript'. return_buffers is a boolean. 
 * Please see {@link https://github.com/mranney/node_redis} (redis.createClient())
 * for further details.
 * @property {Array} stores An array of caches that are currently recognized by the server object. 
 * Note, this is not auto-populated by Redis. It only keeps track of registered cache names.
 * provision of a password.
 * @class
 */
function CacheServer( config ) {
	
	config 			= config || {};
	
	this.host		= config.host || '127.0.0.1';
	this.port		= config.port || 6379;
	this.options	= config.options || { parser: 'hiredis', return_buffers: false };
	this.ttl		= config.ttl || 0;
	this.authorized	= false;
	this.stores		= [];
	
	events.EventEmitter.call(this);

};

require('util').inherits(CacheServer, events.EventEmitter);
CacheServer.prototype.__proto__ = events.EventEmitter.prototype;

/**
 * Register a cache on the server. This creates an attribute of the {@link CacheServer} that can be referenced via "CacheServer.myCache".
 * @param {String} name The name of the cache. Ex: myCache.
 * @param {Number} [ttl] The default time to live for all records in this cache (in seconds). 
 * @return void
 */
CacheServer.prototype.registerStore = function( name, ttl ){
	
	var cache 	= name.replace(/[^\w\s\:]/gi,'').trim(),
		exp		= ttl || this.ttl;
	
	if ( this.stores.indexOf(cache) < 0 ){
		this[cache] = new Cache(this, cache, exp);
		this.stores.push(cache);
	}
}




/**
 * Removes a cache store from the server. This only removes the reference to the cache, not the records within it.
 * @param {String} name The name of the cache to remove.
 * @result void
 */
CacheServer.prototype.unRegisterStore = function( name ){
	
	var cache 	= name.replace(/[^\w\s\:]/gi,'').trim();
	
	if ( this.stores.indexOf(cache) >= 0 ){
		this[cache].close();
		delete this[cache];
		this.stores.splice(this.stores.indexOf(cache),0);
	}
}



/**
 * Removes all of the current caches.
 * @param {Boolean} lazyLoad Remove the cache stores lazily. Setting this to false will persist all of the cache stores.
 * The records in the database will be accessible, but they will be orphaned. Defaults to true.
 * @param {Function} callback An optional callback function. to execute upon completion. 
 * @result void
 */
CacheServer.prototype.flushStores = function( lazyLoad, callback ){
	
	var lazy = lazyLoad || true;
	
	if ( !lazy ) {
		while (this.stores.length > 0) {
			this.stores.purge(true);
		
			this.stores.pop(this.stores.length);
		}
	} else
		this.stores = [];
		
	if (typeof callback === 'function')
		callback();
}



module.exports = exports = CacheServer;