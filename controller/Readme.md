# Controller

The controller directory is home for the server side code, such as NodeJS, BackboneJS, etc.
This folder needs a directory called `node_modules`, which is typically a linked directory created when
using `npm install xxx`.


## modules

The modules directory contains logic code abstracted into modules. The `oauth` directory contains wrappers for `everyauth` (an npm module).
The `lib` directory contains supporting libraries. Additional directories may exist or be created to simplify complex functionality. Any
files located directly in the modules directory are what get included in NodeJS, with the exception of the `index.js` file. This file is
used by Node to load all of the other files into the application.


## routes

Routes are the URL route that displays a page or provides REST functionality. For example, the "/" route (root) may display the main page
while "/validate" may be used to provide user email validation. Routes are bundled into files based on logic. All user routes might be in
a file called user.js while all dictionary functionality (GET/POST/PUT/DELETE) may be in dictionary.js. There is always a file called `index.js`
and `Root.js`. The root file is considered the main file while the index file loads all of the routes into the application.


## util

This is a custom utility folder. It contains wrappers and other helper functions that can be used throughout a Node application.
