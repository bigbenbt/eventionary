/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

mongoose.connect('mongodb://kevin:kevin@staff.mongohq.com:10010/users');

var express = require('express')
  , routes = require('./routes');

var app = module.exports = express.createServer();


//establish database schema

var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;
  
var errCode = new Schema({
    ID			: ObjectId
  , name       	: String
  , def        	: String
  , createdBy  	: String
});

var MyModel = mongoose.model('errCode', errCode);

//configure express

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes
//app.get('/', routes.index);

app.get('/', function(req, res){
    res.render('index.jade', { title: 'My Site' });
});

app.get('/view', function(req, res){
	MyModel.find({}, function (err, doc){
	console.log(doc);
	res.render('view.jade', {title : 'viewing the eventionary'});
	});
});

app.get('/form', function(req, res){
    res.render('form.jade', { title: 'Form' });
});

app.get('/search', function(req,res){
	res.render('search.jade', { title: 'search page'});
});

app.post('/searchResults', function(req, res){
	MyModel.find('req.body.searchType', 'req.body.searchTerm', function (err, doc){
	//console.log(doc);
	res.render('searchResults.jade', { title: 'search results'});
	});	
});

app.post('/process_form',function(req,res){
        console.log(req.body); //This is a JSON object of any <form> inputs and their value
        res.render('about.jade', {title: 'Thanks!'})
		
ObjectId = Schema.ObjectId;

var instance = new MyModel();
instance.name = req.body.name;
instance.def = req.body.definition;
instance.createdBy = req.body.username;
instance.save(function (err) {
});     
});

app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);