var Schema 					= require('mongoose').Schema,
	EmailAddress			= require('./EmailAddress'),
	PhoneNumber				= require('./PhoneNumber'),
	OAuthAccount			= require('./OAuthAccount'),
	PhysicalAddress			= require('./PhysicalAddress');

/**
 * Represents a human being.
 * Contains several virtual attributes for ease of use:
 * - display (display name)
 * - email (the primary email address in user@domain.com format)
 * - phone (primary phone number)
 * - fax (primary fax number)
 * @name Person
 * @version 0.1
 * @class Person
 * @requires mongoose
 * @augments Schema
 * @cfg {Array} eml An array of EmailAddress objects associated with the person.
 * @cfg {String} gn Given name (First name). Defaults to unknown.
 * @cfg {String} sn Surname (last name). Defaults to unknown.
 * @cfg {String} dn Display name. Defaults to first last.
 * @cfg {Date} dt Date created. Defaults to current system time.
 * @cfg {Date} la Last Activity Date . Defaults to current system time.
 * @cfg {String} ttl Professional Title (Job Title). Optional.
 * @cfg {String} dsc Description of the person. Optional.
 * @cfg {Array} phn An array of PhoneNumber objects. Optional.
 * @cfg {Array} addr Addresses of physical locations associated with the person. Optional.
 * @cfg {String} org The name of the organization the person is with. Optional..
 * @cfg {Array} alt An array of Alternate accounts, {@link OAuthkAccount). Objects are associated with the person (ex: Twitter). Optional.
 * @cfg {String} pwd Password. This is an optional field for storing a password. **IMPORTANT** When a password is provided, it is automatically hashed using bcrypt.
 */
exports.Person = Person = new Schema({

	eml	: [EmailAddress],
	gn	: { type: String, 'default':'Unknown' },
	sn	: { type: String, 'default':'Unknown' },
	dn	: String,
	dt	: { type: Date, 'default': Date.now },
	la	: { type: Date, 'default': Date.now },
	ttl	: String,
	dsc	: String,
	phn	: [PhoneNumber],
	addr: [PhysicalAddress],
	alt	: [OAuthAccount],
	pwd	: String

});

//Validation
Person
	.pre('save', function(next){
		
		//Hash any password provided.
		var passwd = this.pwd; 

		if (passwd !== undefined ) {
			
			//If the password doesn't meet minimum specs, throw an error
			if (passwd == null || passwd.trim().length < 7)
				throw Error('Insufficient password.');
				
			var bcrypt = require('bcrypt');
		
			bcrypt.genSalt(7, 25, function(err, salt){
				bcrypt.hash(passwd, salt, function(err, hash){
					
					if (err) throw err;
					
					this.pwd = hash;
					next();
				});
			});
			
		} else 
			next();
	});
	

//Statics



//Virtuals
Person
	.virtual('verified')
	.get( function() {
		
		//If no email address is available, the account has not been verified.
		if (this.eml.length == 0)
			return false;
			
		//Check any existing email addresses until a valid one is found.
		for (var i=0; i<this.eml.length; i++){
			if (this.eml[i].vld)
				return true;
		}
		
		//If no valid address is found, the account is not valid.
		return false;
	});
	
Person
	.virtual('display')
	.get( function() {
		return 	this.dn === undefined 
				|| this.dn == null
				|| this.dn.trim().length == 0 ? this.gn + ' ' + this.sn : this.dn;
	});

Person
	.virtual('email')
	.get( function() {
		for ( var e in this.eml ) {
			if ( this.eml[e].prm )
				return this.eml[e].eml;
		}
		return null;
	});
	
Person
	.virtual('phone')
	.get( function() {
		for ( var e in this.phn ) {
			if ( this.phn[e].typ != 'f' && this.phn[e].prm )
				return this.phn[e].nbr;
		}
		return null;
	});

Person
	.virtual('fax')
	.get( function() {
		for ( var e in this.phn ) {
			if ( this.phn[e].typ == 'f' )
				return this.phn[e].nbr;
		}
		return null;
	});