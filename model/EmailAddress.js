var Schema 			= require('mongoose').Schema;

/**
 * Represents an Email Address.
 * @name EmailAddress
 * @version 0.1
 * @class EmailAddress
 * @requires mongoose
 * @augments Schema
 * @cfg {String} id Question ID.
 * @cfg {String} eml Email address. Required.
 * @cfg {Boolean} prm Primary Address? Required. Defaults to false.
 * @cfg {Boolean} vld Validated Address? Required. Defaults to false.
 * @cfg {Boolean} vcd Validation Code. Optional.
 * @cfg {String} typ Type of email address. Valid options are w= Work (default), p =  Personal, o = Other, ? = Unknwon.
 */
exports.EmailAddress = EmailAddress = new Schema({

	addr: { type: String, required: true, index: true, validate: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/ },
	prm	: { type: Boolean, 'default': false, required: true },
	vld	: { type: Boolean, 'default': false, required: true },
	vcd : { type: String, required: false, index: true },
	vdt : { type: Date, required: false },
	typ	: {
			type:		String,
			index: 		true,
			'enum': 	['w','p','o','?']
	}	

});