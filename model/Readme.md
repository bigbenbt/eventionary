# Model

The model directory contains database schematics using Goose (github.com/coreybutler/goose). This is a wrapper library around Mongoose, an ORM
module for MongoDB.

This directory requires a node_modules directory as created when running `npm install mongoose`.