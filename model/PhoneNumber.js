var Schema 			= require('mongoose').Schema;

/**
 * Represents a phone number.
 * @name PhoneNumber
 * @version 0.1
 * @class PhoneNumber
 * @requires mongoose
 * @augments Schema
 * @property {String} id ID.
 * @property {Number} nbr Number (Digits). Should include country code when possible.
  * @property {Boolean} vld Validated Address? Required. Defaults to false.
 * @property {String} typ Type of email address. Valid options are m = mobile, h = home, o = office, f= fax, ? = unknown/other. Defaults tp Unknown.
 */
exports.PhoneNumber = PhoneNumber = new Schema({

	nbr	: { type: Number, required: true, index: true },
	prm	: { type: Boolean, 'default': false, required: true },
	vld	: { type: Boolean, 'default': false, required: true },
	typ	: {
			type:		String,
			required:	true,
			index: 		true,
			'default':	'?',
			'enum': 	['m','h','o','?','f']
	}	

});