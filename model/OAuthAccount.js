var Schema 			= require('mongoose').Schema;

/**
 * Represents an OAuth account.
 * @name OAuthAccount
 * @version 0.1
 * @class OAuthAccount
 * @requires mongoose
 * @augments Schema
 * @property {String} svc Service Provider. Required.
 * <li>tw = Twitter</li>
 * <li>li = LinkedIn</li>
 * <li>fb = Facebook</li>
 * <li>gh = GitHub</li>
 * @property {String} sid Service ID. Required. This is the ID the service uses to identify the user. 
 * @property {Date} dt Date whe nthe account is created.
 * @property {String} tok Access Token. Optional (Recommended for caching)
 */
exports.OAuthAccount = OAuthAccount = new Schema({

	svc	: {
			type: String,
			required: true,
			index: true,
			'enum': [
				'tw','li','fb','gh'
			]
		},
	sid	: { type: String, required: true, index: true },
	dt	: { type: Date, 'default': Date.now },
	tok	: String

});