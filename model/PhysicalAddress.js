var Schema 			= require('mongoose').Schema;

/**
 * Represents a physical location address
 * @name PhysicalAddress
 * @version 0.1
 * @class PhysicalAddress
 * @requires mongoose
 * @augments Schema
 * @property {String} id ID.
 * @property {String} st Street. Optional.
 * @property {String} c City. Optional.
 * @property {String} r Region (State, Territory, Shire). Optional.
 * @property {String} pc Postal Code/Zip. Optional.
 * @property {String} n Nation/Countrry. Defaults to US.
 * @property {Number} lat Latitude. Optional.
 * @property {Number} lng Longitude. Optional.
 * @property {String} typ Type. o = office, h = home, ? = unknown. Defaults to Office.
 * @prooerty {Boolean} prm Primary address.
 */
exports.PhysicalAddress = PhysicalAddress = new Schema({

	st	: String,
	c	: String,
	r	: String,
	pc	: String,
	n	: { type: String, 'default':'US' },
	lat	: Number,
	lng	: Number,
	typ	: {
		type: String, 
		'default': 'o',
		'enum': ['o','h','?']
	},
	prm	: {type: Boolean, 'default': true }

});