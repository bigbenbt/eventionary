# Eventionary

This repository contains the code for the Eventionary system.


## Exectuing

The recommended way to develop with this code is to use a Linux server with NodeJS and the supervisor npm module `npm install supervisor -g`.
To run the server, execute `NODE_ENV=dev supervisor app.js` from the controller directory on the server. This will monitor the server for changes
to your js files (but **not** ejs files). Every time you upload a new or modified file, it will automatically restart NodeJS for you. 